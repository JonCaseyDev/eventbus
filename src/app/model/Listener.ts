import { Subscriber } from "../interfaces/subscriber.interface";

export class Listener implements Subscriber {
  id: string;
  priority: number;

  constructor(id: string, priority?: number) {
    this.id = id;
    priority == undefined ? (this.priority = 2) : (this.priority = priority);
  }

  handle(msg: any) {
    console.log("Listener<" + this.id + "> handle() ->" + JSON.stringify(msg));
  }

  getPriority(): number {
    return this.priority;
  }

  setPriority(priority): void {
    this.priority = priority;
  }
}
