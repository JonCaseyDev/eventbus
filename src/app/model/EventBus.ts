import { Subscriber } from "../interfaces/subscriber.interface";
import { Topic } from "../interfaces/topic.interface";
import { PubSub } from "../interfaces/pub-sub.interface";

export class EventBus implements PubSub {
  // add type: TopicStore {key:string, value:topic]}
  topics = {};

  constructor() {}

  publish(event: Topic) {
    console.log("event published");
    // If no topic found add topic
    if (!this.topics.hasOwnProperty(event.name)) {
      this.topics[event.name] = event;
      console.log("no prop");
      return;
    }

    // else if topic exists already, add the message to the topic
    this.topics[event.name].message = event.message;
    // iterate through the topic subscribers and broadcast the event
    if (this.topics[event.name].subscribers <= 0) {
      console.log("nosubs");
      return;
    }

    const sortedSubs = this.sortSubscribers(
      this.topics[event.name].subscribers
    );

    console.log("sorted subs", sortedSubs);

    sortedSubs.forEach(listener => {
      console.log("caaling handler for ", listener.id);
      listener.handle(event.message);
    });
  }

  sortSubscribers(subscribers: Subscriber[]): Subscriber[] {
    const subs = subscribers.sort((a: Subscriber, b: Subscriber) => {
      return a.priority - b.priority;
    });
    return subs;
  }

  subscribe(eventName: string, listener: Subscriber) {
    // TODO Check if TopicName exists.
    if (!this.topics.hasOwnProperty(eventName)) {
      console.log("cannot sub to a topic that does not eist");
      return;
    }

    //TODO Listener cannot be added twice.
    const isFound = this.topics[eventName].subscribers.findIndex(
      l => l.id === listener.id
    );

    if (isFound >= 0) return;

    this.topics[eventName].subscribers.push(listener);
  }

  unsubscribe(eventName: string, listener: Subscriber) {
    // looks for the subscriber in the list of subscribers
    const remove = this.topics[eventName].subscribers.findIndex(
      l => l.id === listener.id
    );
    // if not found return
    if (remove === -1) {
      return;
    }
    // else fulfill the unsubscribe request
    this.topics[eventName].subscribers.splice(remove, 1);
  }

  showState() {
    console.log("result set", this.topics);
  }
}

export default EventBus;
