import { Message } from "./../interfaces/message.interface";
import { Listener } from "../model/Listener";
import { Topic } from "../interfaces/topic.interface";

/* Message Data */
let m1: Message<String> = {
  data: 'GOAL! Salah "7  '
};

let m2: Message<String> = {
  data: 'GOAL! Salah "27  '
};

let m3: Message<String> = {
  data: 'GOAL! Salah "47  '
};

let m4: Message<String> = {
  data: 'GOAL! Salah "65  '
};

let m5: Message<String> = {
  data: 'GOAL! Salah "81  '
};

let m6: Message<number> = {
  data: 333
};

let e1: Topic = {
  publisher: "/LiveSports",
  name: "Liverpool",
  message: m1,
  subscribers: []
};

let e2: Topic = {
  publisher: "/LiveSports",
  name: "Liverpool",
  message: m2,
  subscribers: []
};

let e3: Topic = {
  publisher: "/LiveSports",
  name: "Liverpool",
  message: m3,
  subscribers: []
};

let e4: Topic = {
  publisher: "/LiveSports",
  name: "Liverpool",
  message: m4,
  subscribers: []
};

let e5: Topic = {
  publisher: "/LiveSports",
  name: "Liverpool",
  message: m5,
  subscribers: []
};

let e6: Topic = {
  publisher: "/LiveSports",
  name: "Liverpool",
  message: m6,
  subscribers: []
};

let e7: Topic = {
  publisher: "/Programming",
  name: "Javascript",
  message: m1,
  subscribers: []
};

export let MockData = {
  /* Listener Data */
  listener1: new Listener("Jonathan"),
  listener2: new Listener("Klopp", 1),
  listener3: new Listener("Daglish", 1),
  listener4: new Listener("Fergi", 3),
  listener5: new Listener("Owen", 3),

  /*Event Data */
  event1: e1,
  event2: e2,
  evevt3: e3,
  event4: e4,
  event5: e5,
  event6: e6,
  event7: e7
};
