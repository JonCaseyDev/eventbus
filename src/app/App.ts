import { EventBus } from "./model/EventBus";
import { MockData } from "./mock-data/mock-data";

let eventbus = new EventBus();

eventbus.publish(MockData.event1);

eventbus.subscribe("Liverpool", MockData.listener1);
eventbus.subscribe("Liverpool", MockData.listener3);
eventbus.subscribe("Liverpool", MockData.listener5);
eventbus.subscribe("Liverpool", MockData.listener4);
eventbus.subscribe("Liverpool", MockData.listener2);

eventbus.publish(MockData.event1);

eventbus.showState();

// let eventbus = new EventBus();

// eventbus.publish(event1);
// eventbus.unsubscribe("Liverpool", l5);
// eventbus.subscribe("Liverpool", l1);
// eventbus.subscribe("Liverpool", l2);
// eventbus.subscribe("Liverpool", l3);
// eventbus.publish(event2);
// eventbus.subscribe("Liverpool", l4);
// eventbus.publish(event3);
// eventbus.publish(event4);
// eventbus.unsubscribe("Liverpool", l4);
// eventbus.publish(event5);
// eventbus.publish(event6);

// eventbus.subscribe("Liverpool", l1);

// eventbus.showState();
