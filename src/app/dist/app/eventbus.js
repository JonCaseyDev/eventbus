"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/* Move to main */
let m1 = {
    data: 'GOAL! Salah "7  '
};
let m2 = {
    data: 'GOAL! Salah "27  '
};
let m3 = {
    data: 'GOAL! Salah "47  '
};
let m4 = {
    data: 'GOAL! Salah "65  '
};
let m5 = {
    data: 'GOAL! Salah "81  '
};
let m6 = {
    data: 666666666
};
let event1 = {
    publisher: "/LiveSports",
    name: "Liverpool",
    message: m1,
    subscribers: []
};
let event2 = {
    publisher: "/LiveSports",
    name: "Liverpool",
    message: m2,
    subscribers: []
};
let event3 = {
    publisher: "/LiveSports",
    name: "Liverpool",
    message: m3,
    subscribers: []
};
let event4 = {
    publisher: "/LiveSports",
    name: "Liverpool",
    message: m4,
    subscribers: []
};
let event5 = {
    publisher: "/LiveSports",
    name: "Liverpool",
    message: m5,
    subscribers: []
};
let event6 = {
    publisher: "/LiveSports",
    name: "Satan",
    message: m6,
    subscribers: []
};
let l1 = {
    id: "JONATHAN",
    priority: 2,
    handle(msg) { }
};
let l2 = {
    id: "Klopp",
    priority: 1,
    handle(msg) { }
};
let l3 = {
    id: "Daglish",
    priority: 1,
    handle(msg) { }
};
let l4 = {
    id: "Fergi",
    priority: 3,
    handle(msg) { }
};
let l5 = {
    id: "OWEN",
    priority: 3,
    handle(msg) { }
};
class PubSubBus {
    constructor() {
        // https://medium.com/@soffritti.pierfrancesco/create-a-simple-event-bus-in-javascript-8aa0370b3969
        this.topics = {};
    }
    publish(event) {
        // If no topic found add topic
        if (!this.topics.hasOwnProperty(event.name)) {
            this.topics[event.name] = event;
            return;
        }
        // else if topic exists already, add the message to the topic
        this.topics[event.name].message = event.message;
        // iterate through the topic subscribers and broadcast the event
        if (this.topics[event.name].subscribers >= 0) {
            return;
        }
        // else broadcast to all listeners.
        this.topics[event.name].subscribers.forEach(listener => {
            listener.handle(event.message);
        });
    }
    subscribe(eventName, listener) {
        // TODO Check if TopicName exists.
        if (!this.topics.hasOwnProperty(eventName)) {
            return;
        }
        //TODO Listener cannot be added twice.
        const isFound = this.topics[eventName].subscribers.findIndex(l => l.id === listener.id);
        if (isFound >= 0)
            return;
        this.topics[eventName].subscribers.push(listener);
    }
    unsubscribe(eventName, listener) {
        const remove = this.topics[eventName].subscribers.findIndex(l => l.id === listener.id);
        if (remove === -1) {
            return;
        }
        this.topics[eventName].subscribers.splice(remove, 1);
    }
    showState() {
        console.log("result set", this.topics);
    }
}
let bus = new PubSubBus();
bus.publish(event1);
bus.unsubscribe("Liverpool", l5);
bus.subscribe("Liverpool", l1);
bus.subscribe("Liverpool", l2);
bus.subscribe("Liverpool", l3);
bus.publish(event2);
bus.subscribe("Liverpool", l4);
bus.publish(event3);
bus.publish(event4);
bus.unsubscribe("Liverpool", l4);
bus.publish(event5);
bus.publish(event6);
bus.subscribe("Liverpool", l1);
bus.showState();
