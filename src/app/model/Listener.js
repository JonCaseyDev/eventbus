"use strict";
exports.__esModule = true;
var Listener = /** @class */ (function () {
    function Listener(id, priority) {
        this.id = id;
        priority == undefined ? (this.priority = 2) : (this.priority = priority);
    }
    Listener.prototype.handle = function (msg) {
        console.log("Listener<" + this.id + "> handle() ->" + JSON.stringify(msg));
    };
    Listener.prototype.getPriority = function () {
        return this.priority;
    };
    Listener.prototype.setPriority = function (priority) {
        this.priority = priority;
    };
    return Listener;
}());
exports.Listener = Listener;
