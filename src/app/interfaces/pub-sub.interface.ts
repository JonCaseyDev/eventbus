import { Topic } from "./topic.interface";
import { Subscriber } from "./subscriber.interface";

export interface PubSub {
  publish(event: Topic);

  subscribe(eventName: string, listener: Subscriber);

  unsubscribe(eventName: string, listener: Subscriber);
}
