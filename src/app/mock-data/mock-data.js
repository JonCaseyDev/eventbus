"use strict";
exports.__esModule = true;
var Listener_1 = require("../model/Listener");
/* Message Data */
var m1 = {
    data: 'GOAL! Salah "7  '
};
var m2 = {
    data: 'GOAL! Salah "27  '
};
var m3 = {
    data: 'GOAL! Salah "47  '
};
var m4 = {
    data: 'GOAL! Salah "65  '
};
var m5 = {
    data: 'GOAL! Salah "81  '
};
var m6 = {
    data: 333
};
var e1 = {
    publisher: "/LiveSports",
    name: "Liverpool",
    message: m1,
    subscribers: []
};
var e2 = {
    publisher: "/LiveSports",
    name: "Liverpool",
    message: m2,
    subscribers: []
};
var e3 = {
    publisher: "/LiveSports",
    name: "Liverpool",
    message: m3,
    subscribers: []
};
var e4 = {
    publisher: "/LiveSports",
    name: "Liverpool",
    message: m4,
    subscribers: []
};
var e5 = {
    publisher: "/LiveSports",
    name: "Liverpool",
    message: m5,
    subscribers: []
};
var e6 = {
    publisher: "/LiveSports",
    name: "Liverpool",
    message: m6,
    subscribers: []
};
var e7 = {
    publisher: "/Programming",
    name: "Javascript",
    message: m1,
    subscribers: []
};
exports.MockData = {
    /* Listener Data */
    listener1: new Listener_1.Listener("Jonathan"),
    listener2: new Listener_1.Listener("Klopp", 1),
    listener3: new Listener_1.Listener("Daglish", 1),
    listener4: new Listener_1.Listener("Fergi", 3),
    listener5: new Listener_1.Listener("Owen", 3),
    /*Event Data */
    event1: e1,
    event2: e2,
    evevt3: e3,
    event4: e4,
    event5: e5,
    event6: e6,
    event7: e7
};
