import { Message } from "./message.interface";
import { Subscriber } from "./subscriber.interface";

export interface Topic {
  publisher?: string;
  name: string;
  message: Message<any>;
  subscribers: Subscriber[];
}
