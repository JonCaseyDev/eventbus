export interface Subscriber {
  id: string;
  priority: number;
  handle(msg: any);
}
