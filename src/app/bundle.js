(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";
exports.__esModule = true;
var EventBus_1 = require("./model/EventBus");
var mock_data_1 = require("./mock-data/mock-data");
var eventbus = new EventBus_1.EventBus();
// eventbus.publish(MockData.event1);
eventbus.subscribe("Liverpool", mock_data_1.MockData.listener1);
eventbus.subscribe("Liverpool", mock_data_1.MockData.listener3);
eventbus.subscribe("Liverpool", mock_data_1.MockData.listener5);
eventbus.subscribe("Liverpool", mock_data_1.MockData.listener4);
eventbus.subscribe("Liverpool", mock_data_1.MockData.listener2);
eventbus.publish(mock_data_1.MockData.event1);
eventbus.showState();
// let eventbus = new EventBus();
// eventbus.publish(event1);
// eventbus.unsubscribe("Liverpool", l5);
// eventbus.subscribe("Liverpool", l1);
// eventbus.subscribe("Liverpool", l2);
// eventbus.subscribe("Liverpool", l3);
// eventbus.publish(event2);
// eventbus.subscribe("Liverpool", l4);
// eventbus.publish(event3);
// eventbus.publish(event4);
// eventbus.unsubscribe("Liverpool", l4);
// eventbus.publish(event5);
// eventbus.publish(event6);
// eventbus.subscribe("Liverpool", l1);
// eventbus.showState();

},{"./mock-data/mock-data":2,"./model/EventBus":3}],2:[function(require,module,exports){
"use strict";
exports.__esModule = true;
var Listener_1 = require("../model/Listener");
/* Message Data */
var m1 = {
    data: 'GOAL! Salah "7  '
};
var m2 = {
    data: 'GOAL! Salah "27  '
};
var m3 = {
    data: 'GOAL! Salah "47  '
};
var m4 = {
    data: 'GOAL! Salah "65  '
};
var m5 = {
    data: 'GOAL! Salah "81  '
};
var m6 = {
    data: 666666666
};
var e1 = {
    publisher: "/LiveSports",
    name: "Liverpool",
    message: m1,
    subscribers: []
};
var e2 = {
    publisher: "/LiveSports",
    name: "Liverpool",
    message: m2,
    subscribers: []
};
var e3 = {
    publisher: "/LiveSports",
    name: "Liverpool",
    message: m3,
    subscribers: []
};
var e4 = {
    publisher: "/LiveSports",
    name: "Liverpool",
    message: m4,
    subscribers: []
};
var e5 = {
    publisher: "/LiveSports",
    name: "Liverpool",
    message: m5,
    subscribers: []
};
var e6 = {
    publisher: "/LiveSports",
    name: "Satan",
    message: m6,
    subscribers: []
};
exports.MockData = {
    /* Listener Data */
    listener1: new Listener_1.Listener("Jonathan"),
    listener2: new Listener_1.Listener("Klopp", 1),
    listener3: new Listener_1.Listener("Daglish", 1),
    listener4: new Listener_1.Listener("Fergi", 3),
    listener5: new Listener_1.Listener("Owen", 3),
    /*Event Data */
    event1: e1,
    event2: e2,
    evevt3: e3,
    event4: e4,
    event5: e5,
    event6: e6
    //   /* Message Data */
    //   message1: m1,
    //   message2: m2,
    //   message3: m3,
    //   message4: m4,
    //   message5: m5,
    //   message6: m6,
};

},{"../model/Listener":4}],3:[function(require,module,exports){
"use strict";
exports.__esModule = true;
var EventBus = /** @class */ (function () {
    function EventBus() {
        // add type: TopicStore {key:string, value:topic]}
        this.topics = {};
    }
    EventBus.prototype.publish = function (event) {
        console.log("event published");
        // If no topic found add topic
        if (!this.topics.hasOwnProperty(event.name)) {
            this.topics[event.name] = event;
            console.log("no prop");
            return;
        }
        // else if topic exists already, add the message to the topic
        this.topics[event.name].message = event.message;
        // iterate through the topic subscribers and broadcast the event
        if (this.topics[event.name].subscribers <= 0) {
            console.log("nosubs");
            return;
        }
        var sortedSubs = this.sortSubscribers(this.topics[event.name].subscribers);
        console.log("sorted subs", sortedSubs);
        sortedSubs.forEach(function (listener) {
            console.log("caaling handler for ", listener.id);
            listener.handle(event.message);
        });
    };
    EventBus.prototype.sortSubscribers = function (subscribers) {
        var subs = subscribers.sort(function (a, b) {
            return a.priority - b.priority;
        });
        return subs;
    };
    EventBus.prototype.subscribe = function (eventName, listener) {
        // TODO Check if TopicName exists.
        if (!this.topics.hasOwnProperty(eventName)) {
            console.log("cannot sub to a topic that does not eist");
            return;
        }
        //TODO Listener cannot be added twice.
        var isFound = this.topics[eventName].subscribers.findIndex(function (l) { return l.id === listener.id; });
        if (isFound >= 0)
            return;
        this.topics[eventName].subscribers.push(listener);
    };
    EventBus.prototype.unsubscribe = function (eventName, listener) {
        // looks for the subscriber in the list of subscribers
        var remove = this.topics[eventName].subscribers.findIndex(function (l) { return l.id === listener.id; });
        // if not found return
        if (remove === -1) {
            return;
        }
        // else fulfill the unsubscribe request
        this.topics[eventName].subscribers.splice(remove, 1);
    };
    EventBus.prototype.showState = function () {
        console.log("result set", this.topics);
    };
    return EventBus;
}());
exports.EventBus = EventBus;
exports["default"] = EventBus;

},{}],4:[function(require,module,exports){
"use strict";
exports.__esModule = true;
var Listener = /** @class */ (function () {
    function Listener(id, priority) {
        this.id = id;
        priority == undefined ? (this.priority = 2) : (this.priority = priority);
    }
    Listener.prototype.handle = function (msg) {
        console.log("Listener<" + this.id + "> handle() ->" + JSON.stringify(msg));
    };
    Listener.prototype.getPriority = function () {
        return this.priority;
    };
    Listener.prototype.setPriority = function (priority) {
        this.priority = priority;
    };
    return Listener;
}());
exports.Listener = Listener;

},{}]},{},[1]);
