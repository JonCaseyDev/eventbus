"use strict";
exports.__esModule = true;
var EventBus = /** @class */ (function () {
    function EventBus() {
        // add type: TopicStore {key:string, value:topic]}
        this.topics = {};
    }
    EventBus.prototype.publish = function (event) {
        console.log("event published");
        // If no topic found add topic
        if (!this.topics.hasOwnProperty(event.name)) {
            this.topics[event.name] = event;
            console.log("no prop");
            return;
        }
        // else if topic exists already, add the message to the topic
        this.topics[event.name].message = event.message;
        // iterate through the topic subscribers and broadcast the event
        if (this.topics[event.name].subscribers <= 0) {
            console.log("nosubs");
            return;
        }
        var sortedSubs = this.sortSubscribers(this.topics[event.name].subscribers);
        console.log("sorted subs", sortedSubs);
        sortedSubs.forEach(function (listener) {
            console.log("caaling handler for ", listener.id);
            listener.handle(event.message);
        });
    };
    EventBus.prototype.sortSubscribers = function (subscribers) {
        var subs = subscribers.sort(function (a, b) {
            return a.priority - b.priority;
        });
        return subs;
    };
    EventBus.prototype.subscribe = function (eventName, listener) {
        // TODO Check if TopicName exists.
        if (!this.topics.hasOwnProperty(eventName)) {
            console.log("cannot sub to a topic that does not eist");
            return;
        }
        //TODO Listener cannot be added twice.
        var isFound = this.topics[eventName].subscribers.findIndex(function (l) { return l.id === listener.id; });
        if (isFound >= 0)
            return;
        this.topics[eventName].subscribers.push(listener);
    };
    EventBus.prototype.unsubscribe = function (eventName, listener) {
        // looks for the subscriber in the list of subscribers
        var remove = this.topics[eventName].subscribers.findIndex(function (l) { return l.id === listener.id; });
        // if not found return
        if (remove === -1) {
            return;
        }
        // else fulfill the unsubscribe request
        this.topics[eventName].subscribers.splice(remove, 1);
    };
    EventBus.prototype.showState = function () {
        console.log("result set", this.topics);
    };
    return EventBus;
}());
exports.EventBus = EventBus;
exports["default"] = EventBus;
