export interface Message<T> {
  id?: number;
  messageType?: string;
  data: T;
}
